import { APP_IS_LOADING } from '../actionTypes'

const initialState = {
    isLoading: false,
    user:{
        isAuthenticated: false,
        data:{}
    },
    errors:{},
    messages:{},
}


const app = (state = initialState, action) => {
    switch (action.type) {
        case APP_IS_LOADING:
            return {
                ...state,
                user:{
                    ...state.user,
                    newData: true
                },
                isLoading: true
            }
            break

        default:
            return state
            break
    }
}

export default app
