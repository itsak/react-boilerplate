import { APP_IS_LOADING } from "../actionTypes";

export const isLoading = (payload) => {
    return {
        type: APP_IS_LOADING,
        payload
    }
}
