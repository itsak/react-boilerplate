import MainLayout from 'layouts/MainLayout'
import SecondaryLayout from 'layouts/SecondaryLayout'
import Homepage from 'pages/Homepage'
import Aboutpage from 'pages/Aboutpage'
import Servicepage from 'pages/Servicepage'
import Contactpage from 'pages/Contactpage'
import SinglePostpage from 'pages/SinglePostpage'

export const routes = [
    {
        path: '/',
        exact: true,
        component: Homepage,
        layout: MainLayout
    },
    {
        path: '/about',
        exact: true,
        component: Aboutpage,
        layout: MainLayout
    },
    {
        path: '/services',
        exact: true,
        component: Servicepage,
        layout: MainLayout
    },
    {
        path: '/contact',
        exact: true,
        component: Contactpage,
        layout: MainLayout
    },
    {
        path: '/:pageURL',
        exact: true,
        component: SinglePostpage,
        layout: SecondaryLayout
    }
]
