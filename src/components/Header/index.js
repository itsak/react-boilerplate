import React from 'react'
import { Link } from 'react-router-dom'

const Header = () => {
  return (
      <header id="header">
          <Link to="/" className="title">Hyperspace</Link>
          <nav>
              <ul>
                  <li><Link to="/">Welcome</Link></li>
                  <li><Link to="/about">Who we are</Link></li>
                  <li><Link to="/services">What we do</Link></li>
                  <li><Link to="/contact">Get in touch</Link></li>
              </ul>
          </nav>
      </header>
  )
}
export default Header
