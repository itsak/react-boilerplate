import React, { Component } from 'react'
import { Link } from 'react-router-dom'
class Aboutpage extends Component {
  render() {
    return (
        <section id="one" className="wrapper style2 spotlights">
            <section>
                <Link to="/single-post" className="image"><img src="images/pic01.jpg" alt="" data-position="center center" /></Link>
                <div className="content">
                    <div className="inner">
                        <h2>Sed ipsum dolor</h2>
                        <p>Phasellus convallis elit id ullamcorper pulvinar. Duis aliquam turpis mauris, eu ultricies erat malesuada quis. Aliquam dapibus.</p>
                        <ul className="actions">
                            <li><Link to="/single-post" className="button">Learn more</Link></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section>
                <Link to="/single-post" className="image"><img src="images/pic02.jpg" alt="" data-position="top center" /></Link>
                <div className="content">
                    <div className="inner">
                        <h2>Feugiat consequat</h2>
                        <p>Phasellus convallis elit id ullamcorper pulvinar. Duis aliquam turpis mauris, eu ultricies erat malesuada quis. Aliquam dapibus.</p>
                        <ul className="actions">
                            <li><Link to="/single-post" className="button">Learn more</Link></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section>
                <Link to="/single-post" className="image"><img src="images/pic03.jpg" alt="" data-position="25% 25%" /></Link>
                <div className="content">
                    <div className="inner">
                        <h2>Ultricies aliquam</h2>
                        <p>Phasellus convallis elit id ullamcorper pulvinar. Duis aliquam turpis mauris, eu ultricies erat malesuada quis. Aliquam dapibus.</p>
                        <ul className="actions">
                            <li><Link to="/single-post" className="button">Learn more</Link></li>
                        </ul>
                    </div>
                </div>
            </section>
        </section>
    )
  }
}

export default Aboutpage

