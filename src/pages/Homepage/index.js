import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isLoading } from 'store/app/action'
import { Link } from 'react-router-dom'

class Homepage extends Component {
    componentDidMount = () => {
        const { isLoading } = this.props
        isLoading()
    }

  render() {
    return (
        <section id="intro" className="wrapper style1 fullscreen fade-up">
            <div className="inner">
                <h1>Hyperspace</h1>
                <p>Just another fine responsive site template designed by <a href="http://html5up.net">HTML5 UP</a><br />
                    and released for free under the <a href="http://html5up.net/license">Creative Commons</a>.</p>
                <ul className="actions">
                    <li><Link to="/some-more-information" className="button scrolly">Learn more</Link></li>
                </ul>
            </div>
        </section>
    )
  }
}

export default connect( null, { isLoading })(Homepage)
