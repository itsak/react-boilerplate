import React from 'react'
import { Route } from 'react-router-dom'

import Header from 'components/Header'
import Footer from 'components/Footer'

const SecondaryLayout =  ({ component: Component, ...others }) => {
  return (
      <Route {...others} render={matchProps =>(
          <div className="main-layout">
              <Header />
              <div id="wrapper">
                <Component {...matchProps} />
              </div>
              <Footer />
          </div>
      )}/>
  )
}

export default SecondaryLayout
