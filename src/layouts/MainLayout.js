import React from 'react'
import { Route } from 'react-router-dom'

import SideBar from 'components/SideBar'
import Footer from 'components/Footer'

const MainLayout =  ({ component: Component, ...others }) => {
  return (
      <Route {...others} render={matchProps =>(
          <div className="main-layout">
              <SideBar />
              <div id="wrapper">
                <Component {...matchProps} />
              </div>
              <Footer />
          </div>
      )}/>
  )
}

export default MainLayout
