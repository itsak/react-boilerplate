import React, { Component } from 'react'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'

import { routes } from './routes'

import store from './store'

class App extends Component {
  render() {
    return (
        <Provider store={store}>
            <Router>
                <Switch>
                {routes.map((route, index) => {
                    const Layout = route.layout
                    return (
                        <Layout key={index} exact={route.exact} path={route.path} component={route.component} />
                    )
                })}
                </Switch>
            </Router>
        </Provider>
    )
  }
}

export default App
