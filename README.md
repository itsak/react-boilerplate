# React App Boilerplate

## Introduction

This is the boilerplate template to get start with React with Redux and Routers.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

### Included

* React App
* Redux
* React Routers
* Template from [HTML5Up](https://html5up.net/hyperspace)

### Installation

* Clone this repo
`git clone gitlat.com`

* Change Directory to porject Directory and then run
`npm install && npm run start`
To start the development server

* Stop the development server and run
`npm run build` to generate the build folder for production

* You can view the demo site here at [ItsAK](http://react-boilerplate.itsak.in)
